#!/usr/bin/env python3

from argparse import ArgumentParser
from dotenv import load_dotenv
from getpass import getpass
import hashlib
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

__author__          = 'Jonas Ingemarsson'
__copyright__       = 'Copyright 2022'
__version__         = '1.0.0'
__maintainer__      = 'Jonas Ingemarsson'
__email__           = 'ingemarsson.jonas@me.com'
__status__          = 'Testing'
__last_change__     = '2022-12-12'
__dependencies__    = 'selenium, python-dotenv'
__about__           = 'Automated creating of GitHub projects.'
__NOTE__            = 'You need python 3.10+ to run this code due to the use of match/case.'


# Class for handling credentials
class credentials:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    # Checking if given credentials match the hashed credentials in '.env'
    def login(self):
        load_dotenv()
        hashed_username, hashed_password = os.getenv('USERNAME'), os.getenv('PASSWORD')
        if hashed_username == hashlib.sha512(str(self.username).encode('utf-8')).hexdigest() and hashed_password == hashlib.sha512(str(self.password).encode('utf-8')).hexdigest():
            return True
        else:
            return False

# Function to create a git-project using selenium
def create_git(project, desc, private, readme, me):
    # For this to work, you need to install geckodrivers in order to be able to use selenium.
    # https://selenium-python.readthedocs.io/installation.html#drivers
    browser = webdriver.Firefox() # Change this to whatever suits your needs.
    browser.maximize_window()
    browser.get('https://github.com/login')
    browser.find_element(By.CSS_SELECTOR, '#login_field').send_keys(me.username)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(me.password)
    browser.find_element(By.CSS_SELECTOR, '.btn').click()
    browser.find_element(By.CSS_SELECTOR, 'button.btn:nth-child(8)').click()
    browser.find_element(By.CSS_SELECTOR, '#repository_name').send_keys(project)
    match private:
        case True:
            browser.find_element(By.CSS_SELECTOR, '#repository_visibility_private').click()
    match readme:
        case True:
            browser.find_element(By.CSS_SELECTOR, '#repository_auto_init').click()
    browser.find_element(By.CSS_SELECTOR, '#repository_description').send_keys(desc)
    browser.find_element(By.CSS_SELECTOR, '.btn-primary').click()
    browser.find_element(By.CSS_SELECTOR, '.js-feature-preview-indicator-container > summary:nth-child(1)').click()
    time.sleep(0.5)
    browser.find_element(By.CSS_SELECTOR, 'button.dropdown-item:nth-child(2)').click()

def main():
    # Arguments to be used when running the script.
    # All arguments are optional. Using no arguments will create a project named 'no-name'
    # without any description, public and with no readme.
    ap = ArgumentParser()
    ap.add_argument('-p', '--project',
    type=str, nargs='*', 
    help='Name of your project you want to create')
    ap.add_argument('-d', '--description',
    type=str, nargs='?',
    help='a description of the project you are about to create')
    ap.add_argument('-priv', '--private',
    default=False, action='store_true',
    help='Use if you want to make your project private. Default is public')
    ap.add_argument('-r', '--readme',
    default=False, action='store_true',
    help='Use if you want to add a readme when creating your project. Default is False')
    args = ap.parse_args()
    project = (args.project or 'no-name') # Naming the project 'no-name' unless other name is given

    # Checking if credentials match your '.env' file using SHA512
    me = credentials(input('Username: '), getpass())
    if me.login():
        create_git(project, args.description, args.private, args.readme, me)
    else:
        main()


if __name__ == '__main__':
    main()



