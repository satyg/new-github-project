# New GitHub Project

For this script to actually work you need to install the dependencies stated in `requirements.txt`  
```
pip -r requirements.txt
```

You also need to edit `.env` to have your actual credentials for signing in to GitHub. Those credentials must also be hashed to SHA512 to avoid storing plaintext credentials within the script.  
```python
USERNAME=05ee170f46fd6040a23ebd883d63ef3b2aff55e3d6e01eccbc401088d7de0c153251c27e517ea4ad9bed62980366d1a47ceb312a659e77debda650d870094562
PASSWORD=b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86
```
**NOTE**: *The credentials inside the file is username and password, you need to change them*

The script is also preconfigured for using FireFox. This can be changed (look for comments in code). Note that you also need to have drivers installed which you can find [here](https://selenium-python.readthedocs.io/installation.html#drivers).

## Running the script
The scripts takes different arguments, no mandatory, only optional. In case you don't specify any project name, the name will be set to *no-name*.

```python
ap.add_argument('-p', '--project',
type=str, nargs='*', 
help='Name of your project you want to create')

ap.add_argument('-d', '--description',
type=str, nargs='?',
help='a description of the project you are about to create')

ap.add_argument('-priv', '--private',
default=False, action='store_true',
help='Use if you want to make your project private. Default is public')

ap.add_argument('-r', '--readme',
default=False, action='store_true',
help='Use if you want to add a readme when creating your project. Default is False')
```

running the script with below arguments will create a project named *my-new-project* as a private project with a provided readme file from GitHub.

```sh
./main.py -p my-new-project -d "This is the description" --private -r
```